
# +---------------+
# |    BACKEND    |
# +---------------+

import cv2
import numpy as np
import sys as SYSTEM
import os as SOFTWARE
from matplotlib import pyplot as plt
import optparse
import imutils
import time
from colorama import init, Fore, Back, Style

init(autoreset=True)



parser = optparse.OptionParser()
parser.add_option("-i", "--file", dest="filename",
                  help="Image to use (png only!)", metavar="PNG_FILE")
(options, args) = parser.parse_args()

img = cv2.imread(options.filename)

if img is None:
    print(Back.RED + Fore.BLACK + 'ERROR : backend.py requires an option !')
    time.sleep(1)
    print('Usage: cuberegognisio [OPTION]')
    print('Run "cuberegognisio --help" for more information')
    SYSTEM.exit()
else:
    pass

resized = imutils.resize(img, width=300)
ratio = img.shape[0] / float(resized.shape[0])

trisNumber=0
cubes=0
pentagonNumber=0
circleNumber=0
rectangleNumber=0
print("Identifying ...")
class ShapeDetector():
    def __init__(self):
        pass
    def detect(self, c):
        global cubes
        global trisNumber
        global pentagonNumber
        global circleNumber
        global rectangleNumber
        # initialize the shape name and approximate the contour
        shape = "unidentified"
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.04 * peri, True)
        # if the shape is a triangle, it will have 3 vertices
        if len(approx) == 3:
            trisNumber+=1
            shape = "triangle"
    	# if the shape has 4 vertices, it is either a square or
    	# a rectangle
        elif len(approx) == 4:
    	    # compute the bounding box of the contour and use the
    	    # bounding box to compute the aspect ratio
            (x, y, w, h) = cv2.boundingRect(approx)
            ar = w / float(h)
    	    # a square will have an aspect ratio that is approximately
    	    # equal to one, otherwise, the shape is a rectangle
            shape = "square" if ar >= 0.95 and ar <= 1.05 else "rectangle"
            if shape == "square":
                cubes+=1
            else:
                rectangleNumber+=1
        # if the shape is a pentagon, it will have 5 vertices
        elif len(approx) == 5:
            shape = "pentagon"
            pentagonNumber+=1
        # otherwise, we assume the shape is a circle
        else:
            shape = "circle"
            circleNumber+=1
        # return the name of the shape
        return shape

# convert the resized image to grayscale, blur it slightly,
# and threshold it
grey_img = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(grey_img, (5, 5), 0)
thresh_img = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
 
# applying cv2.THRESH_BINARY thresholding techniques
ret, thresh_img = cv2.threshold(grey_img, 245, 255, cv2.THRESH_BINARY)
 
# write the Output image
cv2.imwrite('Output.png',thresh_img)

contours, hierarchy = cv2.findContours(image=thresh_img, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)

font=cv2.FONT_HERSHEY_COMPLEX


# find contours in the thresholded image and initialize the
# shape detector
cnts = cv2.findContours(thresh_img.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
sd = ShapeDetector()

# loop over the contours
#for c in cnts:
shapeNumber=0
for idx,c in enumerate(cnts):
    # compute the center of the contour, then detect the name of the
    # shape using only the contour
    M = cv2.moments(c)
    cX = int((M["m10"] / M["m00"]) * ratio)
    cY = int((M["m01"] / M["m00"]) * ratio)
    shaper = sd.detect(c)
    # multiply the contour (x, y)-coordinates by the resize ratio,
    # then draw the contours and the name of the shape on the image
    c = c.astype("float")
    c *= ratio
    c = c.astype("int")
    cv2.drawContours(img, [c], -1, (0, 255, 0), 2)
    cv2.putText(img, shaper, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
    	0.5, (0, 0, 0), 2)
    # show the output image (DISABLED)
    cv2.imshow("Image", img)
    cv2.waitKey(0)
    shapeNumber+=1
    print(shapeNumber, " shapes")

if shapeNumber <=2:
    print(Back.RED + Fore.BLACK + "ERROR : NOT VALID ! Aborting ...")
    SYSTEM.exit()
elif shapeNumber > 2 and shapeNumber <= 6:
    print(Back.BLUE + Fore.WHITE + "Info : VALID !")
    if rectangleNumber >= 2 and cubes <= 4:
        print(Back.GREEN + Fore.BLACK + "Result : 'Pavé droit' !")
    elif cubes == 6:
        print(Back.GREEN + Fore.BLACK + "Result : 'Cube' !")
    elif trisNumber == 3 and rectangleNumber == 1 or trisNumber == 3 and cubes == 1:
        print(Back.GREEN + Fore.BLACK + "Result : 'Pyramide à base Rectangulaire' !")
    elif trisNumber == 4:
        print(Back.GREEN + Fore.BLACK + "Result : 'Pyramide à base triangle' !")
    elif rectangleNumber == 1 and circleNumber == 2:
        print(Back.GREEN + Fore.BLACK + "Result : 'Cylindre' !")
    elif rectangleNumber <= 4:
        print(Back.GREEN + Fore.BLACK + "Result : 'Prisme droit' !")
    else:
        print(Back.RED + Fore.BLACK + "ERROR : Sorry but an error happened !")
else:
    print(Back.RED + Fore.BLACK + "ERROR : INVALID ! If it is indeed valid, please report this bug ! Not all of the features has been added yet !")


SOFTWARE.remove("Output.png")
